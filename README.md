1. File input:
Là file biểu diễn ma trận mìn theo cách 1. File có dạng
1 0 0 1 1 0 1 0 1
0 1 0 1 0 1 0 0 1
1 1 0 0 1 1 1 0 0
1 0 1 0 1 0 1 0 1
0 1 1 0 0 1 0 0 1
1 0 0 0 1 0 1 0 1
2. File output:
Là file biểu diễn ma trận mìn theo cách 2. File này là để so sánh với file 1 khi biểu diễn cùng 1 ma trận mìn. File có dạng
1 2 3 2 3 3 1 2 1
4 3 4 3 6 5 3 4 1
3 4 4 4 4 5 2 3 2
3 6 3 4 3 6 3 3 1
3 4 2 4 3 4 2 5 2
1 3 2 2 1 3 1 2 1
3. Cách chạy test chương trình
- Mở thư mục chứa file exe. Hiện tại file đang có trong đường dẫn \BaiToanDoMin\ConsoleApp1\bin\Debug\netcoreapp3.1
- Trên URL, gõ cmd để mở command
- Trong command, chạy lệnh sau
ConsoleApp1.exe <path file input> <path file output>
- Trong đó, thay path file input và file output đã tạo để so sánh
- Ví dụ:
ConsoleApp1.exe D:\codework\BaiToanDoMin\input.txt D:\codework\BaiToanDoMin\output.txt
4. Kiểm tra kết quả trả về